package uk.ac.uos.i2jp;

public class Example {
	private String name;

	public Example(String name) {
		this.name = name;
	}
	
	public String getName() {
		if (null == name) return "nothing";
		return name;
	}
	
	public void brag() {
		System.out.println(getName() + " rules!");
	}
	
	public static void main(String[] args) {
		String name = args.length > 0 ? args[0] : "Java";
		Example ex = new Example(name);
		ex.brag();
	}
}
