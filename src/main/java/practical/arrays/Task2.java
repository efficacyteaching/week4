package practical.arrays;

import java.util.ArrayList;

public class Task2 {
	public static void main(String[] args) {
		ArrayList<Integer> sums = new ArrayList<>(36);
		for (int i = 1; i <= 6; ++i) {
			for (int j = 1; j <= 6; ++j) {
				sums.add(i + j);
			}
		}
		
		for (Integer n : sums) {
			System.out.print(n + " ");
		}
		System.out.println();
	}
}
