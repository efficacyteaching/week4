package practical.arrays;

public class Task4 {
	public static void main(String[] args) {
		int[][] matrix = new int[][] {
			{ 1, 2,	3 },
			{ 4, 5, 6 }
		};
				
		int[][] m2 = transpose(matrix);
		print(m2);
	}
	
	private static int[][] transpose(int[][] matrix) {
	    int m = matrix.length;
	    int n = matrix[0].length;

	    int[][] ret = new int[n][m];

	    for(int x = 0; x < n; x++) {
	        for(int y = 0; y < m; y++) {
	            ret[x][y] = matrix[y][x];
	        }
	    }

	    return ret;
	}
	
	private static void print(int[][] matrix) {
		for (int[] row : matrix) {
			for (int cell : row) {
				System.out.printf("%3d", cell);
			}
			System.out.println();
		}
	}
}
