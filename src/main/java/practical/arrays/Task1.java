package practical.arrays;

public class Task1 {
	public static void main(String[] args) {
		int[] numbers = { 17, 89, 27, 11, 91, 42, 73, 90, 5, 56 };
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		int total = 0;

		for (int n : numbers) {
			if (n < min) min = n;
			if (n > max) max = n;
			total += n;
		}
		
		int range = max - min;
		float mean = total / numbers.length;
		
		float sqtotal = 0;
		for (int n : numbers) {
			float sq = (n - mean) * (n - mean);
			sqtotal += sq;
		}
		
		double sd = Math.sqrt(sqtotal / (numbers.length - 1));
		
		System.out.println("min => " + min + " max => " + max + " range => " + range + " mean => " + mean + " sd => " + sd);
	}
}
