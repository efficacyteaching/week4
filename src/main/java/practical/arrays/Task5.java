package practical.arrays;

public class Task5 {
	public static void main(String[] args) {
		int[][] a = new int[][] {
			{ 2, 3, 4 },
			{ 1, 0, 1 },
			{ 4, 3, 2 }
		};

		int[][] b = new int[][] {
			{ 5, 3, 5 },
			{ 1, 0, 1 },
			{ 3, 2, 3 }
		};
				
		int[][] product = multiply(a, b);
		print(product);
	}
	
	private static int[][] multiply(int[][] a, int[][] b) {
		int rows = a.length;
		int cols = b[0].length;
		int[][] ret = new int[rows][cols];
		for (int i=0; i < rows ; ++i) {
		    for (int j=0; j < cols; ++j) {
		      for (int k=0; k < rows; ++k) {
			        ret[i][j] += a[i][k] * b[k][j];
		      }
		    }
		}
	    return ret;
	}
	
	private static void print(int[][] matrix) {
		for (int[] row : matrix) {
			for (int cell : row) {
				System.out.printf("%3d", cell);
			}
			System.out.println();
		}
	}
}
