package test;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import uk.ac.uos.i2jp.Example;

public class ExampleTest {
	Example ex;
	
	@Test
	public void testGetName() {
		ex = new Example("Testing");
		assertEquals("Testing", ex.getName());
	}
	
	@Test
	public void testNull() {
		ex = new Example(null);
		assertEquals("nothing", ex.getName());
	}
	
	@Test
	public void testStdout() {
		ex = new Example("Testing");
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		PrintStream old = System.out;
		PrintStream capture = new PrintStream(buf);
		System.setOut(capture);
		
		ex.brag();
		
		System.setOut(old);
		assertEquals("Testing rules!" + System.getProperty("line.separator"), buf.toString());
	}
}
